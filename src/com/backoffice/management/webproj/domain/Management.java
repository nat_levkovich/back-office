package com.backoffice.management.webproj.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.test.api.converter.JpaMoneyConverter;
import com.backoffice.common.Money;
import com.backoffice.management.webproj.domain.enumeration.State;
import com.backoffice.management.webproj.json.MoneySerializer;

/**
 * A Management.
 */
@Entity
@Table(name = "management")
public class Management implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "description")
    private String description;

    @NotNull
    @JsonSerialize(using = MoneySerializer.class)
    @Convert(converter = JpaMoneyConverter.class)
    @Column(name = "amountValue", nullable = false)
    private Money amountValue;

    @NotNull
    @Column(name = "amount_curr_code", nullable = false)
    private String amountCurrCode;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "state", nullable = false)
    private State state;

    @OneToMany(mappedBy = "management")
    @JsonIgnore
    private Set<Client> clients = new HashSet<>();

    @OneToMany(mappedBy = "parentManagement")
    @JsonIgnore
    private Set<Management> subManagements = new HashSet<>();

    @ManyToOne
    private Management parentManagement;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Management name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public Management description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Money getAmountValue() {
        return amountValue;
    }

    public void setAmountValue(Money amountValue) {
        this.amountValue = amountValue;
    }

    public Management amountValue(Money amountValue) {
        this.amountValue = amountValue;
        return this;
    }

    public String getAmountCurrCode() {
        return amountCurrCode;
    }

    public Management amountCurrCode(String amountCurrCode) {
        this.amountCurrCode = amountCurrCode;
        return this;
    }

    public void setAmountCurrCode(String amountCurrCode) {
        this.amountCurrCode = amountCurrCode;
    }

    public State getState() {
        return state;
    }

    public Management state(State state) {
        this.state = state;
        return this;
    }

    public void setState(State state) {
        this.state = state;
    }

    public Set<Client> getClients() {
        return clients;
    }

    public Management clients(Set<Client> clients) {
        this.clients = clients;
        return this;
    }

    public Management addClient(Client client) {
        clients.add(client);
        return this;
    }

    public Management removeClient(Client client) {
        clients.remove(client);
        return this;
    }

    public void setClients(Set<Client> clients) {
        this.clients = clients;
    }


    public Set<Management> getSubManagements() {
        return subManagements;
    }

    public Management subManagements(Set<Management> managements) {
        this.subManagements = managements;
        return this;
    }

    public Management addManagement(Management management) {
        subManagements.add(management);
        return this;
    }

    public Management removeManagement(Management management) {
        subManagements.remove(management);
        return this;
    }

    public void setSubManagements(Set<Management> managements) {
        this.subManagements = managements;
    }

    public Management getParentManagement() {
        return parentManagement;
    }

    public Management parentManagement(Management management) {
        this.parentManagement = management;
        return this;
    }

    public void setParentManagement(Management management) {
        this.parentManagement = management;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Management management = (Management) o;
        if(management.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, management.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Management{" +
            "id=" + id +
            ", name='" + name + "'" +
            ", description='" + description + "'" +
            ", amountValue='" + amountValue + "'" +
            ", amountCurrCode='" + amountCurrCode + "'" +
            ", state='" + state + "'" +
            '}';
    }
}
