package com.backoffice.management.webproj.domain;


import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.test.api.converter.JpaMoneyConverter;
import com.backoffice.common.Money;
import com.backoffice.management.webproj.domain.enumeration.State;
import com.backoffice.management.webproj.json.MoneySerializer;
import org.hibernate.validator.constraints.Email;

/**
 * A Client.
 */
@Entity
@Table(name = "client")
public class Client implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "external_id", nullable = false)
    private Integer externalId;

    @NotNull
    @Column(name = "password", nullable = false)
    private String password;

    @NotNull
    @Column(name = "currency_id", nullable = false)
    private Integer currencyId;

    @NotNull
    @Column(name = "first_name", nullable = false)
    private String firstName;

    @NotNull
    @Column(name = "last_name", nullable = false)
    private String lastName;

    @NotNull
    @Column(name = "post_code", nullable = false)
    private String postCode;

    @NotNull
    @Column(name = "country_id", nullable = false)
    private Integer countryId;

    @NotNull
    @Email
    @Column(name = "email", nullable = false)
    private String email;

    @JsonSerialize(using = MoneySerializer.class)
    @Convert(converter = JpaMoneyConverter.class)
    @Column(name = "balanceClient", nullable = false)
    private Money balanceClient;

    @Column(name = "amount_curr_code", nullable = false)
    private String amountCurrCode;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "state", nullable = false)
    private State state;

    @NotNull
    @Column(name = "birth_date", nullable = false)
    private String birthDate;

    @NotNull
    @Column(name = "nickname", nullable = false)
    private String nickname;

    @ManyToOne
    private Management management;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPassword() {
        return password;
    }

    public Client password(String password) {
        this.password = password;
        return this;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getCurrencyId() {
        return currencyId;
    }

    public Client currencyId(Integer currencyId) {
        this.currencyId = currencyId;
        return this;
    }

    public void setCurrencyId(Integer currencyId) {
        this.currencyId = currencyId;
    }

    public String getFirstName() {
        return firstName;
    }

    public Client firstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public Client lastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPostCode() {
        return postCode;
    }

    public Client postCode(String postCode) {
        this.postCode = postCode;
        return this;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public Integer getCountryId() {
        return countryId;
    }

    public Client countryId(Integer countryId) {
        this.countryId = countryId;
        return this;
    }

    public void setCountryId(Integer countryId) {
        this.countryId = countryId;
    }

    public String getEmail() {
        return email;
    }

    public Client email(String email) {
        this.email = email;
        return this;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Money getBalanceClient() {
        return balanceClient;
    }

    public Client balanceClient(Money balanceClient) {
        this.balanceClient = balanceClient;
        return this;
    }

    public void setBalanceClient(Money balanceClient) {
        this.balanceClient = balanceClient;
    }

    public String getAmountCurrCode() {
        return amountCurrCode;
    }

    public Client amountCurrCode(String amountCurrCode) {
        this.amountCurrCode = amountCurrCode;
        return this;
    }

    public void setAmountCurrCode(String amountCurrCode) {
        this.amountCurrCode = amountCurrCode;
    }

    public State getState() {
        return state;
    }

    public Client state(State state) {
        this.state = state;
        return this;
    }

    public void setState(State state) {
        this.state = state;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public Client birthDate(String birthDate) {
        this.birthDate = birthDate;
        return this;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getNickname() {
        return nickname;
    }

    public Client nickname(String nickname) {
        this.nickname = nickname;
        return this;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public Management getManagement() {
        return management;
    }

    public Client management(Management management) {
        this.management = management;
        return this;
    }

    public void setManagement(Management management) {
        this.management = management;
    }

    public Integer getExternalId()
    {
        return externalId;
    }


    public void setExternalId(Integer externalId)
    {
        this.externalId = externalId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Client client = (Client) o;
        if(client.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, client.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Client{" +
            "id=" + id +
            ",externalId=" + externalId +
            ", password='" + password + "'" +
            ", currencyId='" + currencyId + "'" +
            ", firstName='" + firstName + "'" +
            ", lastName='" + lastName + "'" +
            ", postCode='" + postCode + "'" +
            ", countryId='" + countryId + "'" +
            ", email='" + email + "'" +
            ", balanceClient='" + balanceClient + "'" +
            ", amountCurrCode='" + amountCurrCode + "'" +
            ", state='" + state + "'" +
            ", birthDate='" + birthDate + "'" +
            ", nickname='" + nickname + "'" +
            '}';
    }
}
