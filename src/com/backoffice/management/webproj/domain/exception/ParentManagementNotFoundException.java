package com.backoffice.management.webproj.domain.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class ParentManagementNotFoundException extends RuntimeException {
    private final long id;

    public ParentManagementNotFoundException(long id) {
        super("Can't find management by id: " + id);
        this.id = id;
    }

    public long getId() {
        return id;
    }
}
