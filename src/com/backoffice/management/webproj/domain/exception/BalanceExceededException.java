package com.backoffice.management.webproj.domain.exception;


import com.backoffice.common.Money;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;


@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class BalanceExceededException extends RuntimeException
{
    private final Money currentManagementAmount;

    private final Money enteredAmount;


    /**
     * Constructs a new runtime exception with {@code null} as its detail message. The cause is not initialized, and may
     * subsequently be initialized by a call to {@link #initCause}.
     */
    public BalanceExceededException(Money currentManagementAmount, Money enteredAmount)
    {
        super("Current balance:" + currentManagementAmount + " is less than entered balance:" + enteredAmount);
        this.currentManagementAmount = currentManagementAmount;
        this.enteredAmount = enteredAmount;
    }


    public Money getEnteredAmount()
    {
        return enteredAmount;
    }


    public Money getCurrentManagementAmount()
    {
        return currentManagementAmount;
    }
}
