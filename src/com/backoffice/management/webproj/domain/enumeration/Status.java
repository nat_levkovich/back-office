package com.backoffice.management.webproj.domain.enumeration;


public enum Status {
    OK, ERROR, WAIT
}
