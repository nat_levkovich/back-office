package com.backoffice.management.webproj.domain.enumeration;

/**
 * The State enumeration.
 */
public enum State {
    OPEN,SUSPEND,CLOSED
}
