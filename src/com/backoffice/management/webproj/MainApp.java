package com.backoffice.management.webproj;

import com.backoffice.management.webproj.domain.Management;
import com.backoffice.common.Money;
import com.backoffice.management.webproj.config.Constants;

import com.backoffice.management.webproj.domain.Management;
import com.backoffice.management.webproj.domain.enumeration.State;
import com.backoffice.management.webproj.repository.ManagementRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.actuate.autoconfigure.*;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.Collection;

@ComponentScan(basePackages = "com.backoffice.management.webproj")
@EnableJpaRepositories
public class MainApp {
    public static final Integer countryId = 1;
    private static final Logger log = LoggerFactory.getLogger(MainApp.class);

    @Inject
    private Environment env;

    @Autowired
    private ManagementRepository managementRepository;


    @PostConstruct
    public void initApplication() {
        log.info("Running with Spring profile(s) : {}", Arrays.toString(env.getActiveProfiles()));
        Collection<String> activeProfiles = Arrays.asList(env.getActiveProfiles());
        if (activeProfiles.contains(Constants.SPRING_PROFILE_DEVELOPMENT) && activeProfiles.contains(Constants.SPRING_PROFILE_PRODUCTION)) {
            log.error("You have misconfigured your application! It should not run " +
                "with both the 'dev' and 'prod' profiles at the same time.");
        }

        /*Add root management*/
        Management rootManagement = new Management();
        rootManagement.setName("root Management");
        rootManagement.setAmountValue(new Money(10_000.99));
        rootManagement.setAmountCurrCode("EUR");
        rootManagement.setState(State.OPEN);
        rootManagement.setDescription("Predefined root Management in back office");

        managementRepository.save(rootManagement);
    }

    /**
     * Main method, used to run the application.
     *
     * @param args the command line arguments
     * @throws UnknownHostException if the local host name could not be resolved into an address
     */
    public static void main(String[] args) throws UnknownHostException {
        SpringApplication app = new SpringApplication(MainApp.class);
        DefaultProfileUtil.addDefaultProfile(app);
        Environment env = app.run(args).getEnvironment();
        log.info("\n----------------------------------------------------------\n\t" +
                "Application '{}' is running! Access URLs:\n\t" +
                "Local: \t\thttp://127.0.0.1:{}\n\t" +
                "External: \thttp://{}:{}\n----------------------------------------------------------",
            env.getProperty("spring.application.name"),
            env.getProperty("server.port"),
            InetAddress.getLocalHost().getHostAddress(),
            env.getProperty("server.port"));

    }

}
