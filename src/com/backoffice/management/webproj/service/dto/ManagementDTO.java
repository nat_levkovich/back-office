package com.backoffice.management.webproj.service.dto;

import com.backoffice.common.Money;
import com.backoffice.management.webproj.domain.enumeration.State;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the Management entity.
 */
public class ManagementDTO implements Serializable {

    private Long id;

    @NotNull
    private String name;

    private String description;

    @NotNull
    private Money amountValue;

    private String amountCurrCode;

    @NotNull
    private State state;

    private Long parentManagementId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Money getAmountValue() {
        return amountValue;
    }

    public void setAmountValue(Money amountValue) {
        this.amountValue = amountValue;
    }

    public String getAmountCurrCode() {
        return amountCurrCode;
    }

    public void setAmountCurrCode(String amountCurrCode) {
        this.amountCurrCode = amountCurrCode;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public Long getParentManagementId() {
        return parentManagementId;
    }

    public void setParentManagementId(Long managementId) {
        this.parentManagementId = managementId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ManagementDTO managementDTO = (ManagementDTO) o;

        if (!Objects.equals(id, managementDTO.id)) return false;

        return true;
    }


    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "ManagementDTO{" +
            "id=" + id +
            ", name='" + name + '\'' +
            ", description='" + description + '\'' +
            ", amountValue=" + amountValue +
            ", amountCurrCode='" + amountCurrCode + '\'' +
            ", state=" + state +
            ", parentManagementId=" + parentManagementId +
            '}';
    }
}
