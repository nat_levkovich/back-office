package com.backoffice.management.webproj.service.mapper;

import com.backoffice.management.webproj.domain.Management;
import com.backoffice.management.webproj.service.dto.ManagementDTO;

import org.mapstruct.*;
import java.util.List;

/**
 * Mapper for the entity Management and its DTO ManagementDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ManagementMapper {

    @Mapping(source = "parentManagement.id", target = "parentManagementId")
    ManagementDTO managementToManagementDTO(Management management);

    List<ManagementDTO> managementsToManagementDTOs(List<Management> managements);

    @Mapping(target = "clients", ignore = true)
    @Mapping(target = "subManagements", ignore = true)
    @Mapping(source = "parentManagementId", target = "parentManagement")
    Management managementDTOToManagement(ManagementDTO managementDTO);

    List<Management> managementDTOsToManagements(List<ManagementDTO> managementDTOs);

    default Management managementFromId(Long id) {
        if (id == null) {
            return null;
        }
        Management management = new Management();
        management.setId(id);
        return management;
    }
}
