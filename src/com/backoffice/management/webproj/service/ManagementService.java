package com.backoffice.management.webproj.service;

import com.backoffice.management.webproj.service.dto.ManagementDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for leading Management.
 */
public interface ManagementService {

    /**
     * Save a management.
     *
     * @param managementDTO the entity to save
     * @return the persisted entity
     */
    ManagementDTO save(ManagementDTO managementDTO);

    /**
     * Create a new sub management.
     *
     * @param managementDTO the entity to save
     * @return the persisted entity
     */
    ManagementDTO createSubManagement(ManagementDTO managementDTO);

    /**
     *  Get all the managements.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<ManagementDTO> findAll(Pageable pageable);

    /**
     *  Get the "id" management.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    ManagementDTO findOne(Long id);

    /**
     *  Delete the "id" management.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);
}
