package com.backoffice.management.webproj.service.impl;

import com.backoffice.common.Money;
import com.backoffice.management.webproj.domain.exception.BalanceExceededException;;
import com.backoffice.management.webproj.domain.Management;
import com.backoffice.management.webproj.domain.exception.ParentManagementNotFoundException;
import com.backoffice.management.webproj.repository.ManagementRepository;
import com.backoffice.management.webproj.service.ManagementService;
import com.backoffice.management.webproj.service.dto.ManagementDTO;
import com.backoffice.management.webproj.service.mapper.ManagementMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import javax.inject.Inject;

/**
 * Service Implementation for leading Management.
 */
@Service
@Transactional
public class ManagementServiceImpl implements ManagementService {

    private final Logger log = LoggerFactory.getLogger(ManagementServiceImpl.class);

    @Inject
    private ManagementRepository managementRepository;

    @Inject
    private ManagementMapper managementMapper;

    /**
     * Save a management.
     *
     * @param managementDTO the entity to save
     * @return the persisted entity
     */
    public ManagementDTO save(ManagementDTO managementDTO) {
        log.debug("Request to save Management : {}", managementDTO);
        Management management = managementMapper.managementDTOToManagement(managementDTO);
        management = managementRepository.save(management);
        ManagementDTO result = managementMapper.managementToManagementDTO(management);
        return result;
    }

    @Transactional
    public ManagementDTO createSubManagement(ManagementDTO managementDTO) {
        Management parentManagement = managementRepository.findOne(managementDTO.getParentManagementId());
        if (parentManagement == null) {
            throw new ParentManagementNotFoundException(managementDTO.getParentManagementId());
        }
        Management subManagement = managementMapper.managementDTOToManagement(managementDTO);
        subManagement.setParentManagement(parentManagement);

        Money delta = parentManagement.getAmountValue().sub(subManagement.getAmountValue());
        if (delta.lessThan(Money.ZERO)) {
            throw new BalanceExceededException(parentManagement.getAmountValue(), subManagement.getAmountValue());
        }

        parentManagement.setAmountValue(delta);

        managementRepository.save(parentManagement);
        managementRepository.save(subManagement);
        return managementMapper.managementToManagementDTO(subManagement);
    }

    /**
     *  Get all the managements.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ManagementDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Managements");
        Page<Management> result = managementRepository.findAll(pageable);
        return result.map(management -> managementMapper.managementToManagementDTO(management));
    }

    /**
     *  Get one management by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public ManagementDTO findOne(Long id) {
        log.debug("Request to get Management : {}", id);
        Management management = managementRepository.findOne(id);
        ManagementDTO managementDTO = managementMapper.managementToManagementDTO(management);
        return managementDTO;
    }

    /**
     *  Delete the  management by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Management : {}", id);
        managementRepository.delete(id);
    }
}
