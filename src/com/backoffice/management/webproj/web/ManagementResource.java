package com.backoffice.management.webproj.web;

import com.codahale.metrics.annotation.Timed;
import com.backoffice.management.webproj.domain.Management;
import com.backoffice.management.webproj.repository.ManagementRepository;
import com.backoffice.management.webproj.service.ManagementService;
import com.backoffice.management.webproj.service.dto.ManagementDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for leading management.
 */
@RestController
@RequestMapping("/api")
public class ManagementResource {

    private final Logger log = LoggerFactory.getLogger(ManagementResource.class);

    @Inject
    private ManagementService managementService;

    @Autowired
    private ManagementRepository managementRepository;

    /**
     * POST  /managements : Create a new management.
     *
     * @param managementDTO the managementDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new managementDTO, or with status 400 (Bad Request) if the management has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/managements",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ManagementDTO> createManagement(@Valid @RequestBody ManagementDTO managementDTO) throws URISyntaxException {
        log.debug("REST request to save management : {}", managementDTO);
        Management parent = managementRepository.findOne(managementDTO.getParentManagementId());
        managementDTO.setAmountCurrCode(parent.getAmountCurrCode());
        ManagementDTO result = managementService.createSubManagement(managementDTO);
        return ResponseEntity.created(new URI("/api/managements/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert("management", result.getId().toString()))
                .body(result);
    }

    /**
     * PUT  /managements : Updates an existing management.
     *
     * @param managementDTO the managementDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated managementDTO,
     * or with status 400 (Bad Request) if the managementDTO is not valid,
     * or with status 500 (Internal Server Error) if the managementDTO couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/managements",
            method = RequestMethod.PUT,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ManagementDTO> updateManagement(@Valid @RequestBody ManagementDTO managementDTO) throws URISyntaxException {
        log.debug("REST request to update management : {}", managementDTO);
        if (managementDTO.getId() == null) {
            return createManagement(managementDTO);
        }
        ManagementDTO result = managementService.save(managementDTO);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert("management", managementDTO.getId().toString()))
                .body(result);
    }

    /**
     * GET  /managements : get all the managements.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of managements in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/managements",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<ManagementDTO>> getAllManagements(Pageable pageable)
            throws URISyntaxException {
        log.debug("REST request to get a page of managements");
        Page<ManagementDTO> page = managementService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/managements");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /managements/:id : get the "id" management.
     *
     * @param id the id of the managementDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the managementDTO, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/managements/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ManagementDTO> getManagement(@PathVariable Long id) {
        log.debug("REST request to get management : {}", id);
        ManagementDTO managementDTO = managementService.findOne(id);
        return Optional.ofNullable(managementDTO)
                .map(result -> new ResponseEntity<>(
                        result,
                        HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /managements/:id : delete the "id" management.
     *
     * @param id the id of the managementDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/managements/{id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteManagement(@PathVariable Long id) {
        log.debug("REST request to delete management : {}", id);
        managementService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("management", id.toString())).build();
    }

}

