package com.backoffice.management.webproj.repository;

import com.backoffice.management.webproj.domain.Management;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;


/**
 * Spring Data JPA repository for the Management entity.
 */
@SuppressWarnings("unused")
public interface ManagementRepository extends JpaRepository<Management,Long> {
    Management findManagementByName(@Param("name") String name);

}
