package com.backoffice.management.webproj.json;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.backoffice.common.Money;

import java.io.IOException;
import java.util.HashMap;

public class MoneySerializer extends JsonSerializer<Money> {
    @Override
    public void serialize(Money value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
        HashMap<String, Object> map = new HashMap<>();
        map.put("value", value.getValue()/Money.SCALE);
        map.put("cents", value.getCents());

        gen.writeObject(map);
    }
}
